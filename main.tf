resource "google_compute_address" "external_ip" {
  name = "jitsi-meet-ip"
  description = "Static ip address for the jitsi meet vm instance"
}

resource "google_compute_firewall" "firewall_rules" {
  name    = "jitsi"
  network = "default"

  allow {
    protocol = "udp"
    ports = ["10000"]
  }

  source_tags = ["jitsi"]
}

resource "google_dns_record_set" "dns_entry" {
  name = "${var.domain}."
  type = "A"
  ttl = 300

  managed_zone = var.managed_zone

  rrdatas = [google_compute_address.external_ip.address]
}

data "google_compute_image" "vm_image" {
  family  = "debian-10"
  project = "debian-cloud"
}

resource "google_compute_instance" "vm_instance" {
  name         = "jitsi-meet-instance"
  machine_type = "e2-standard-4"
  hostname = var.domain

  boot_disk {
    initialize_params {
      image = data.google_compute_image.vm_image.self_link
    }
  }

  tags = ["http-server", "https-server", "jitsi", ]

  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.external_ip.address
    }
  }

  metadata_startup_script = <<EOT
    sudo -i
    apt-get install -y wget apt-transport-https
    echo 'deb https://download.jitsi.org stable/' >> /etc/apt/sources.list.d/jitsi-stable.list
    wget -qO -  https://download.jitsi.org/jitsi-key.gpg.key | sudo apt-key add -
    apt-get update
    echo "jitsi-videobridge jitsi-videobridge/jvb-hostname string ${var.domain}" | debconf-set-selections
    echo "jitsi-meet-web-config jitsi-meet/cert-choice select 'Generate a new self-signed certificate (You will later get a chance to obtain a Let's encrypt certificate)'" | debconf-set-selections
    apt-get --option=Dpkg::Options::=--force-confold --option=Dpkg::options::=--force-unsafe-io --assume-yes --quiet install jitsi-meet
    mkdir -p /etc/letsencrypt/renewal-hooks/deploy/
    touch /etc/letsencrypt/renewal-hooks/deploy/0000-coturn-certbot-deploy.sh
    chmod +x /etc/letsencrypt/renewal-hooks/deploy/0000-coturn-certbot-deploy.sh
    echo "${var.email}" | /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
  EOT
}
