module "jitsi-gcp" {
    source = "../"

    # This is going to override whatever you have as defaults in the variables.
    project = "This"
    domain = "That"
    region = "Something"
    zone = "zone"
    managed_zone = "m_zone"
    email = "your@email.com"
}
