variable "project" {
  type = string
  description = "This is the gcp project name (automatically set)"
}

variable "domain" {
  type = string
  description = "This is the domain (automatically set)"
}

variable "region" {
  type = string
  description = "This is the gcp region (automatically set)"
}

variable "zone" {
  type = string
  description = "This is the gcp zone (automatically set)"
}

variable "managed_zone" {
  type = string
  description = "This is the cloud dns managed zone (automatically set)"
}

variable "email" {
  type = string
  description = "This is the email address for let's encrypt (automatically set)"
}
