# Host Jitsi On Google Cloud Plattform

First of all: special thanks to Jochen Kirstätter for his [guide](https://jochen.kirstaetter.name/install-jitsi-meet-on-gcp/)!

This project is for hosting your own jitsi instance on the google cloud plattform.
I use it for hosting my [GitLab Meetups](https://www.meetup.com/de-DE/GitLab-Meetup-Hamburg/).
Previously I used the hosted variant of jitsi on https://meet.jit.si .
But at some point, too much people wanted to attend to a meetup and I choose the option to host it on my own.
I also picked the Google Cloud Plattform for it, because I am familiar to it and they provide a free contigent of resources.
After the meetup it showed that this is a good alternative to other products like zoom.
The performance was awesome and everything went great.
So I decided to use terraform for automation and this is the result!

## Usage
1. To start the Jitsi Instance you have to execute the pipeline via web.  
2. After around 5 minutes Jitsi is up to operate (if nothing went wrong).
3. To stop the Jitsi Instance you need to execute the `destroy jitsi` job.

## Setup
The setup is a little specific to my environment.
I have another project where I host a Google Cloud DNS. 
This project is based on this assumption. 

### Google Cloud Apis
- Compute Engine API
- Cloud DNS API

### GitLab Project
If you want to fork this project to use it on your own you need following CI/CD variables:
- `GOOGLE_APPLICATION_CREDENTIALS`: Your account credentials for the [Google Cloud Plattform](https://cloud.google.com/)
- `TF_VAR_domain`: Your domain where you host your Jitsi instance (example = `meet.example.com`)
- `TF_VAR_email`: This is your email for the [Let's Encrypt](https://letsencrypt.org/) Certificate (example = `your@email.com`)
- `TF_VAR_location`: The Google Cloud Region of your choice (example = `EU`)
- `TF_VAR_managed_zone`: The name of your Google Cloud DNS managed zone (example = `example-managed-zone`)
- `TF_VAR_project`: The name of your Google Cloud Project (example = `example-project`)
- `TF_VAR_region`: The Google Cloud Region of your choice (example = `europe-west1`)
- `TF_VAR_zone`: The Google Cloud Zone of your choice (example = `europe-west1-c`)

### Terraform
In the `backend.tf` file you have to change the project id to your GitLab project id.

If you don't want to use Google Cloud DNS you should delete the resource `"google_dns_record_set" "dns_entry"`.
You have to fetch the ip of your instance via the Google Cloud Console or define an output in terraform.
